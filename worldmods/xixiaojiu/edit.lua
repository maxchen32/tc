-- GENERATED CODE
-- Node Box Editor, version 0.8.1 - Glass
-- Namespace: test

minetest.register_node("test:dizuo", {
	tiles = {
		"baked_clay_black.png",
		"baked_clay_black.png",
		"baked_clay_black.png",
		"baked_clay_black.png",
		"baked_clay_black.png",
		"xixiaojiu_front.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.25, -0.5, -0.375, 0.25, -0.125, 0.375}, -- NodeBox1
			{-0.375, -0.125, -0.375, 0.375, 0.5, 0.375}, -- NodeBox2
			{0.3125, -0.5, 0.125, 0.5, -0.1875, 0.4375}, -- NodeBox3
			{-0.5, -0.5, 0.125, -0.3125, -0.1875, 0.4375}, -- NodeBox4
			{-0.5, -0.4375, 0.1875, 0.5, -0.25, 0.375}, -- NodeBox5
		}
	}
})

