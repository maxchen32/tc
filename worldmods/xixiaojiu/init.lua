-- GENERATED CODE
-- Node Box Editor, version 0.8.1 - Glass
-- Namespace: test

minetest.register_node("xixiaojiu:dizuo", {
    description = "xixiaojiu",
    inventory_image = "xixiaojiu_front.png",
	tiles = {
		"xixiaojiu_black.png",
		"xixiaojiu_black.png",
		"xixiaojiu_black.png",
		"xixiaojiu_black.png",
		"xixiaojiu_black.png",
		"xixiaojiu_front.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
        fixed = {
			{-0.25, -0.5, -0.375, 0.25, -0.125, 0.375}, -- NodeBox1
			{-0.375, -0.125, -0.375, 0.375, 0.5, 0.375}, -- NodeBox2
			{0.3125, -0.5, 0.125, 0.5, -0.1875, 0.4375}, -- NodeBox3
			{-0.5, -0.5, 0.125, -0.3125, -0.1875, 0.4375}, -- NodeBox4
			{-0.5, -0.4375, 0.1875, 0.5, -0.25, 0.375}, -- NodeBox5
		}
	},
    groups = {cracky = 3}
})
minetest.register_node("xixiaojiu:head", {
    description = "xixiaojiu head",
    inventory_image = "xixiaojiu_head.png",

	tiles = {
		"xixiaojiu_up.png",
		"xixiaojiu_black.png",
		"xixiaojiu_black.png",
		"xixiaojiu_black.png",
		"xixiaojiu_black.png",
		"xixiaojiu_head.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.375, -0.5, -0.375, 0.375, -0.125, 0.375}, -- NodeBox1
			{-0.5, -0.125, -0.5, 0.5, -0.0625, 0.5}, -- NodeBox2
		}
	},
    groups = {cracky = 3}
})

